package com.progressoft.devops.assignment.controller;

import com.progressoft.devops.assignment.repository.PersonEntity;
import com.progressoft.devops.assignment.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

import static com.progressoft.devops.assignment.controller.PersonMapper.fromEntity;
import static org.springframework.http.ResponseEntity.notFound;

@RestController
@RequestMapping("/persons")
@Slf4j
public class PersonController {


    public PersonController(PersonRepository repository) {
        this.repository = repository;
    }

    private final PersonRepository repository;

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody @Valid Person person, UriComponentsBuilder builder) {
        log.info("received a request to create a person");
        PersonEntity entity = repository.save(PersonMapper.toEntity(person));
        UriComponents uriComponents = builder.path("/persons/{id}").buildAndExpand(entity.getId());
        return ResponseEntity.created(uriComponents.toUri()).body(person.getName());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody @Valid Person person) {
        log.info("received a request to update a person");
        Optional<PersonEntity> existingEntity = repository.findById(id);
        if (!existingEntity.isPresent())
            return notFound().build();
        if (person.getId() != null && !id.equals(person.getId()))
            return ResponseEntity.badRequest().body("Ids mismatch");
        repository.save(PersonMapper.toEntity(person, id));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        log.info("received a request to delete a person");
        Optional<PersonEntity> existingEntity = repository.findById(id);
        if (!existingEntity.isPresent())
            return notFound().build();
        repository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/")
    public ResponseEntity<Page<Person>> list(@PageableDefault Pageable pageable) {
        log.info("received a request to list persons");
        return ResponseEntity.ok(repository.findAll(pageable)
                .map(PersonMapper::fromEntity));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> get(@PathVariable Long id) {
        log.info("received a request to retrieve a person");
        Optional<PersonEntity> result = repository.findById(id);
        return result.map(personEntity -> ResponseEntity.ok(fromEntity(personEntity))).orElseGet(() -> notFound().build());
    }
}
