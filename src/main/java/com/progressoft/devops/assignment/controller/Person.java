package com.progressoft.devops.assignment.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
public class Person {
    private final Long id;
    @NotEmpty
    private final String name;
    @Min(1)
    private final int age;
}
